{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### はじめに\n",
    "\n",
    "正方行列 $A\\in\\mathbb{C}^{n\\times n}$ と実数 $\\alpha\\in(0,1)$ に対して， $A^\\alpha$ は $A^\\alpha := \\exp(\\alpha\\log(A))$ と定義される．\n",
    "ただし，行列の指数関数は $ \\exp(X) := I + X + \\frac{1}{2!}X^2 + \\frac{1}{3!}X^3 + \\cdots \\; (X\\in\\mathbb{C}^{n\\times n}) $ と定義され，行列の（主）対数関数は $\\log(A) :\\Leftrightarrow Y\\in\\mathbb{C}^{n\\times n}\\colon \\exp(Y) = A, \\; \\Lambda(Y) \\subset \\{z\\in\\mathbb{C} \\colon |\\mathrm{Im}(z)| < \\pi\\}$ と定義される．\n",
    "\n",
    "### このノートブックの目的\n",
    "\n",
    "目的は，[Alg. 2, 1] を用いて正定値対称行列 $A$ に対する $A^\\alpha$ を計算することである．\n",
    "アルゴリズムの中で複素数を入力するJacobiの楕円関数を計算する必要があるが，ここでは [Ellilptic.jl](https://github.com/nolta/Elliptic.jl) と [2, Eqs (16.2.1) -- (16.21.4)] を用いて計算する．\n",
    "\n",
    "[1] Hale, Nicholas; Higham, Nicholas J.; Trefethen, Lloyd N. Computing $\\mathbf{A}^\\alpha$, $\\log(\\mathbf{A})$, and related matrix functions by contour integrals. SIAM J. Numer. Anal. 46 (2008), no. 5, 2505--2523.\n",
    "\n",
    "[2] Abramowitz, Milton; Stegun, Irene A. Handbook of mathematical functions with formulas, graphs, and mathematical tables. National Bureau of Standards Applied Mathematics Series, 55 For sale by the Superintendent of Documents, U.S. Government Printing Office, Washington, D.C."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "using LinearAlgebra\n",
    "import Elliptic"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "ellipj (generic function with 2 methods)"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ellipj(u::T, m) where {T<:Real} = Elliptic.ellipj(u, m)\n",
    "\n",
    "\n",
    "function ellipj(z::T, m) where {T<:Complex}\n",
    "    x = real(z)\n",
    "    y = imag(z)\n",
    "\n",
    "    s, c, d = ellipj(x, m)\n",
    "    s1, c1, d1 = ellipj(y, 1-m)\n",
    "\n",
    "    denom = c1^2 + m*s^2*s1^2\n",
    "    sn = (s*d1 + im*c*d*s1*c1) / denom\n",
    "    cn = (c*c1 - im*s*d*s1*d1) / denom\n",
    "    dn = (d*c1*d1 - im*m*s*c*s1) / denom\n",
    "\n",
    "    return sn, cn, dn\n",
    "end"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "powm_cauchy"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "\"\"\"\n",
    "    powm_cauchy(A, α, m, λmax, λmin)\n",
    "\n",
    "Computing the matrix fractional power A^α for an SPD matrix A on the basis of [Alg. 2, 1].\n",
    "\n",
    "- m is the number of abscissas.\n",
    "- λmax and λmin are approximates of the maximum eingevalue and the minimum eigenvalue, respectively.\n",
    "\n",
    "[1] Hale, Nicholas; Higham, Nicholas J.; Trefethen, Lloyd N.\n",
    "Computing 𝐀^α, log(𝐀), and related matrix functions by contour integrals.\n",
    "SIAM J. Numer. Anal. 46 (2008), no. 5, 2505--2523.\n",
    "\"\"\"\n",
    "function powm_cauchy(A, α, m, λmax, λmin)\n",
    "    κ = λmax/λmin\n",
    "    k = (κ^(1/4) - 1) / (κ^(1/4) + 1)\n",
    "    K = Elliptic.K(k^2)\n",
    "    Kp = Elliptic.K(1 - k^2)\n",
    "\n",
    "    t = collect(1:m) .- 0.5\n",
    "    t = @. im/2*Kp - K + t*2*K/m\n",
    "    u, cn, dn = zero(t), zero(t), zero(t)\n",
    "    for i = 1:m\n",
    "        u[i], cn[i], dn[i] = ellipj(t[i], k^2)\n",
    "    end\n",
    "\n",
    "    w = @. (λmax*λmin)^(1/4) * (1/k+u) / (1/k-u)\n",
    "    dzdt = @. cn * dn / (1/k-u)^2\n",
    "\n",
    "    S = zeros(ComplexF64, size(A))\n",
    "    for j = 1:m\n",
    "        S += w[j]^(2α-1) * inv(w[j]^2*I - A) * dzdt[j]\n",
    "    end\n",
    "    S = -8*K*(λmax*λmin)^(1/4) * imag.(S) * A / (k*π*m)\n",
    "    \n",
    "    return S\n",
    "end"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "(m, relerr) = (2, 0.06224806223604075)\n",
      "(m, relerr) = (4, 0.00039739301298502463)\n",
      "(m, relerr) = (6, 5.358100697727232e-5)\n",
      "(m, relerr) = (8, 5.573571522572922e-7)\n",
      "(m, relerr) = (10, 1.7494399813681127e-8)\n",
      "(m, relerr) = (12, 9.776731790388115e-10)\n",
      "(m, relerr) = (14, 9.94948723472175e-12)\n",
      "(m, relerr) = (16, 4.358060954211044e-13)\n",
      "(m, relerr) = (18, 1.2911875924521339e-14)\n",
      "(m, relerr) = (20, 2.0248405768994306e-15)\n"
     ]
    }
   ],
   "source": [
    "Exact = [2 1 0; 1 2 1; 0 1 2]\n",
    "A = Exact^3\n",
    "λmax = eigmax(A)\n",
    "λmin = eigmin(A)\n",
    "α = 1/3\n",
    "\n",
    "for m = 2:2:20\n",
    "    X = powm_cauchy(A, α, m, λmax, λmin)\n",
    "    relerr = norm(X - Exact) / norm(Exact)\n",
    "    @show m, relerr\n",
    "end"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Julia 1.5.1",
   "language": "julia",
   "name": "julia-1.5"
  },
  "language_info": {
   "file_extension": ".jl",
   "mimetype": "application/julia",
   "name": "julia",
   "version": "1.5.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
